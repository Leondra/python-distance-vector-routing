This project uses socket programming in python to implement a simplified version of the Distance Vector Routing Protocol.

INSTALLATION
-Edit the topology files (topo1.txt, topo2.txt, topo3.txt, topo4.txt) and replace the IP addresses provided with the IP addresses of the
devices that you will be using to run each server
-Download and install the latest version of Python: https://www.python.org/
-Enter the directory of the program using the command "cd <file path>"

RUNNING THE PROGRAM
Once the latest version of Python is installed and the user has entered the directory of the program, run the following command to get started:
"python route.py"

Note: this program makes use of Unix Sockets, and should be run using a POSIX-compatible operating system or runtime environment.

Once the program is running, follow the prompts that appear to enter further commands.

CONTRIBUTIONS
Elizabeth Morgan: Handled file handling, threading, code troubleshooting and error handling, and the main menu.
Jessirae Bufford: Handled crash server, display routing, sort connections, sort routing and wrote the README file.
Ryan Kelly: Handled find neighbors, connect neighbors, service connection, step update, compile DV, Bellman Ford and routing update.
