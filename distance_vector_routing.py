import fcntl
import os
import selectors
import socket
import sys
import threading
from threading import Semaphore

# GLOBALS
CONNECTION_LIST = {}  # server-ID : (IP, Port)
NEIGHBOR_DVECTORS = {}  # dictionary of distance vectors for neighbors
inf_cost = float('inf')  # positive infinity for link costs
LINK_COSTS = {}  # cost (indexed by neighbor ID)
MY_ID = 0  # server-ID of the server running on this process
MY_NEIGHBORS = {}  # server-ID : [IP, Port, sockID], includes each server that is a neighbor to this server
PACKETS_RECEIVED = 0 # Total number of routing updates received from neighbors since the last "packets" command
MISSED_PACKETS = {} # Number of routing packets missing in a row from each neighbor
MY_DVECTOR = {}  # [next hop, cost] (indexed by destination ID) A next-hop of -1 indicates that there is no path
SERVER_NUM = 0  # number of servers

EVENTS = selectors.EVENT_READ | selectors.EVENT_WRITE
sel = selectors.DefaultSelector()
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

""" Reads data from the topology file for this server and uses it to populate the CONNECTION_LIST, LINK_COSTS
and MY_DVECTOR global dictionaries.

:param readfile: The filename of the topology file for this server
"""

def resolveTopology(readfile):
    global SERVER_NUM
    global CONNECTION_LIST
    global MY_DVECTOR
    global LINK_COSTS
    global MY_ID
    global NEIGHBOR_DVECTORS
    try:
        file = open(readfile)
        lines = file.readlines()
        file.close()
    except:
        print("\nInvalid filename.\n")
        sys.exit()
    SERVER_NUM = lines[0].rstrip("\n")
    edgeNum = lines[1].rstrip("\n")
    for i in range(int(SERVER_NUM)):
        temp = lines[i + 2].split(" ")
        temp.pop(0)
        CONNECTION_LIST[(i + 1)] = (temp[0], (temp[1].rstrip("\n")))
    for i in range(int(edgeNum)):
        if MY_ID == 0:
            MY_ID = int(lines[i + (2 + int(SERVER_NUM))][0])
        temp = lines[i + (2 + int(SERVER_NUM))].split(" ")
        temp.pop(0)
        temp = (int(temp[0]), (temp[1].rstrip("\n")))
        integerTemp = temp[0]
        if temp[1] == 'inf':
            integerTemp2 = inf_cost
        else:
            integerTemp2 = int(temp[1])
        LINK_COSTS[integerTemp] = integerTemp2
    for key, value in LINK_COSTS.items():
        MY_DVECTOR[key] = [key, value] # next hop is initialized as neighbor, and cost as the link to neighbor
    for key, value in CONNECTION_LIST.items():
        if key not in MY_DVECTOR:
            if key != MY_ID:
                MY_DVECTOR[key] = [-1, inf_cost]
    MY_DVECTOR[MY_ID] = [MY_ID, 0]
    # could use handling for dupe IPs&Port combination

"""Sets up the listener server.
"""

def bootServer():
    global CONNECTION_LIST
    IP = CONNECTION_LIST[MY_ID][0]
    Port = CONNECTION_LIST[MY_ID][1]
    sock.bind((IP, int(Port)))
    sock.listen()
    sock.setblocking(False)
    sel.register(sock, selectors.EVENT_READ, data="listener")
    findNeighbors()

"""Uses the topology file for this server to locate the neighbors of this server (servers with a link to this server),
and populates the MY_NEIGHBORS, NEIGHBOR_DVECTORS, and MISSED_PACKETS global dictionaries.
"""

def findNeighbors():
    global MY_ID
    global CONNECTION_LIST
    global MY_NEIGHBORS
    global MISSED_PACKETS
    for key, _ in LINK_COSTS.items():
        listValues = list(CONNECTION_LIST[key])
        listValues.append(-1)
        MY_NEIGHBORS[key] = listValues
    initialTable = {}
    for i in range(1, int(SERVER_NUM) + 1):
        initialTable[i] = inf_cost
    for key in MY_NEIGHBORS:
        NEIGHBOR_DVECTORS[key] = initialTable
        MISSED_PACKETS[key] = inf_cost
    connectNeighbors()

"""Attempts to connect to the neighbor servers. If successful, sends a message indicating the server ID of this server.
"""

def connectNeighbors():
    global MY_NEIGHBORS
    global MY_ID
    print("Connecting. . .\n")
    for key, values in MY_NEIGHBORS.items():
        if isinstance(values, list):
            lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                lsock.connect((values[0], int(values[1])))
                sel.register(lsock, EVENTS, data="neighbor")
                values[2] = lsock
                message = "myid " + str(MY_ID)
                sendMessage(key, message)  # send our server ID to the server we connected to
            except ConnectionRefusedError:
                pass

"""Accepts an incoming connection from another server.

:param sock: The socket of the incoming connection
"""

def accept_wrapper(sock):
    conn, addr = sock.accept()
    conn.setblocking(False)
    sel.register(conn, EVENTS, data="neighbor")

"""Sends a message to another server.

:param serverID: The server ID of the message recipient
:param message: The message that will be senderTuple
:type message: str
"""

def sendMessage(serverID, message):
    _, _, lsock = MY_NEIGHBORS.get(serverID)
    if (lsock != -1):
        try:
            lsock.send(message.encode())
        except ConnectionError:
            pass

"""Receives from the listener socket. Possible outcomes include cleaning up after a server connection hangs up,
matching a server ID to a new connection, handling a routing update from another server, and responding to an 
updated link cost.

:param key: The listener socket
:param mask: A bitwise mask returned by the selector, indicating which I/O events should be waited for on a given file object
:param semaphore: A binary semaphore that will be passed to the bellmanFord() method in order to protect a critical section
"""

def service_connection(key, mask, semaphore):
    global MY_NEIGHBORS
    global NEIGHBOR_DVECTORS
    global PACKETS_RECEIVED
    sock = key.fileobj
    if mask & selectors.EVENT_READ:
        try:
            recv_data = sock.recv(1024)  # Should be ready to read
            if len(recv_data) == 0: # connection hung up
                for key, values in MY_NEIGHBORS.items():
                    if values[2] == sock:
                        MY_NEIGHBORS.pop(key)
                        NEIGHBOR_DVECTORS.pop(key)
                        LINK_COSTS.pop(key)
                        MISSED_PACKETS[key] = inf_cost
                        break
                sel.unregister(sock)
                sock.shutdown(socket.SHUT_RDWR)
                sock.close()
                bellmanFord(semaphore)
                return
            recv_data = recv_data.decode()
            if  recv_data.startswith("myid"):  # recv_data contains ServerID of sender
                recv_list = recv_data.split(" ")
                recv_list.pop(0)
                senderID = int(recv_list[0])
                MY_NEIGHBORS[senderID][2] = sock
            elif recv_data.startswith("link"): # recv_data contains updated link cost
                recv_list = recv_data.split(" ")
                recv_list.pop(0)
                senderID = int(recv_list[0])
                if recv_list[1] == 'inf':
                    updateCost = inf_cost
                else:
                    updateCost = int(recv_list[1])
                LINK_COSTS[senderID] = updateCost
                print("\nupdate SUCCESS\n")
                bellmanFord(semaphore)
            else: # recv_data contains RoutingUpdate
                PACKETS_RECEIVED += 1
                compiledTuple = compileDV(recv_data) # (Distance vector, senderID)
                vector = compiledTuple[0]
                senderID = compiledTuple[1]
                if MISSED_PACKETS[senderID] == inf_cost:
                    MISSED_PACKETS[senderID] = 0
                elif MISSED_PACKETS[senderID] > 0:
                    MISSED_PACKETS[senderID] -= 1
                print("RECEIVED A MESSAGE FROM SERVER " + str(senderID) + "\n")
                NEIGHBOR_DVECTORS[senderID] = vector
                bellmanFord(semaphore)
        except ConnectionError:
            pass

"""A helper function for linkUpdate() that processes user input.

:param update: The user input for the "update" command
:param semaphore: A binary semaphore that will be passed to linkUpdate and then passed to the bellmanFord() method in order 
to protect a critical section
"""

def linkUpdateHelper(update, semaphore):  # <command> <server-id1> <server-id2> <link cost>
    global MY_ID
    update = update.split(" ")
    update.pop(0)  # remove 'update' command
    if len(update) > 3:
        print("\nupdate ERROR: Too many arguments. Refer to \"help\".\n")
        return
    elif int(update[0]) != MY_ID and int(update[1]) != MY_ID:
        print("\nupdate ERROR: One of the IDs must belong to this server.\n")
    elif len(update) < 3:
        print("\nupdate ERROR: Too few arguments. Refer to \"help\".\n")
        return
    elif not update[0].isnumeric():
        print("\nupdate ERROR: Source server ID is INVALID.\n")
        return
    elif not update[1].isnumeric():
        print("\nupdate ERROR: Destination server ID is INVALID.\n")
        return
    if update[2] == "inf":
        update[2] = inf_cost  # updates to positive infinity
    elif not update[2].isnumeric():
            print("\nupdate ERROR: Bad link cost. Try an integer or \'inf\'.\n")
            return
    if update[2] != inf_cost:
        update[2] = int(update[2])
    linkUpdate(update, semaphore)

"""Updates the cost of a link.

:param update: Sanitized user input
:param semaphore: A binary semaphore that will be passed to the bellmanFord() method in order to protect a critical section
"""

def linkUpdate(update, semaphore):
    global MY_DVECTOR
    if int(update[0]) == MY_ID:
        notMyID = update[1]
    else:
        notMyID = update[0]
    for key, values in LINK_COSTS.items():
        if key == int(notMyID):
            LINK_COSTS[key] = update[2]
            message = "link " + str(MY_ID) + " " + str(update[2])
            sendMessage(key, message)
            print("\nupdate SUCCESS\n")
            bellmanFord(semaphore)
            return
    if notMyID == update[1]:
        print("\nupdate ERROR: destination ID does not belong a neighbor of this server.\n")
    else:
        print("\nupdate ERROR: source ID does not belong a neighbor of this server.\n")

"""Sends a routing update to neighboring servers. Manually called by the "step" command.
"""

def stepUpdate():
    global MY_NEIGHBORS
    update = RoutingUpdate()
    for key, values in MY_NEIGHBORS.items():
        if isinstance(values, list):
            if values[2] != -1:
                sendMessage(key, update)
    print("\nstep SUCCESS\n")

"""Sends a routing update to neighboring servers. Automatically called by the timer. If three routing updates have been missed in a row
from a server, that server's connection will be closed.

:param semaphore: A binary semaphore used to protect a critical section
"""

def intervalUpdate(semaphore):
    global MY_NEIGHBORS
    global MISSED_PACKETS
    update = RoutingUpdate()
    for key, values in MY_NEIGHBORS.items():
        if isinstance(values, list):
            if values[2] != -1:
                sendMessage(key, update)
    for key, value in MISSED_PACKETS.items():
        if value != inf_cost:
            value += 1
        if value == 3:
            semaphore.acquire() #critical section start
            lsock = MY_NEIGHBORS[key][2]
            MY_NEIGHBORS.pop(key)
            NEIGHBOR_DVECTORS.pop(key)
            LINK_COSTS.pop(key)
            sel.unregister(lsock)
            lsock.shutdown(socket.SHUT_RDWR)
            lsock.close()
            semaphore.release() #critical section end
            print("\nServer " + str(key) + " removed due to inactivity.\n")

"""Displays the number of routing updates received since the latest "packets" command.
"""

def displayPackets():
    global PACKETS_RECEIVED
    print("\nPackets received since last \"packets\" command: " + str(PACKETS_RECEIVED))
    PACKETS_RECEIVED = 0
    print("packets SUCCESS\n")

"""Displays the server's routing table.
"""

def displayRouting():
    sortRouting()  # Sort routing table before printing table
    theList = '\nServerID:\tNext Hop\tCost'  # Print sorted routing table
    print(theList)
    print("-------------------------------------")
    for key in MY_DVECTOR:
        if key != MY_ID:
            if MY_DVECTOR[key][0] == -1:
                nextHop = "No path"
            else:
                nextHop = str(MY_DVECTOR[key][0])
            print(str(key) + "\t\t" + nextHop + "\t\t" + str(MY_DVECTOR[key][1]))
    print("display SUCCESS\n")

"""Disables a link to a neighboring server, closing the connection.

:param update: User input for the "disable" command
:param semaphore: A binary semaphore that will be passed to the bellmanFord() method in order to protect a critical section
"""

def disableLink(update, semaphore):
    global LINK_COSTS
    global MY_DVECTOR
    update = update.split(" ")
    update.pop(0)
    update = update[0]
    if len(update) != 1:
        print("\ndisable ERROR: Please provide an existing serverID to disable.\n")
        return
    if update == MY_ID:
        print("\ndisable ERROR: You cannot disable yourself. Try <crash> to sever all connections.\n")
        return
    for key, values in MY_NEIGHBORS.items():
        if int(key) == int(update):
            lsock = values[2]
            MY_NEIGHBORS.pop(key)
            NEIGHBOR_DVECTORS.pop(key)
            LINK_COSTS.pop(key)
            MISSED_PACKETS[key] = inf_cost
            sel.unregister(lsock)
            lsock.shutdown(socket.SHUT_RDWR)
            lsock.close()
            #message = "link " + str(MY_ID) + " inf"
            #sendMessage(key, message)
            print("\ndisable SUCCESS\n")
            bellmanFord(semaphore)
            return
    print("\ndisable ERROR: ID does not belong to a neighbor of this server.\n")

"""Exits the program to simulate a server crash.
"""

def crashServer():
    timer.cancel()
    sys.exit()

"""Assembles a distance vector from a neighbor's routing update, using the same format as MY_DVECTOR.

:param update: A routing update
:type update: str
:return: A tuple containing the newly-assembled distance vector and the Server ID of the neighbor that senderTuple
the routing update
"""

def compileDV(update):
    global MY_ID
    DV = {}
    updateList = update.split(" ")
    updateList.pop(0)
    port = updateList.pop(0)
    ip = updateList.pop(0)
    senderTuple = (ip, port)
    numServers = int(len(updateList) / 4)
    for i in range(numServers):
        serverID = int(updateList[2 + (i * 4)])
        if updateList[3 + (i * 4)] != "inf":
            cost = int(updateList[3 + (i * 4)])
        else: cost = inf_cost
        DV[serverID] = cost
    senderID = list(CONNECTION_LIST.keys())[list(CONNECTION_LIST.values()).index(senderTuple)]
    returnTuple = (DV, senderID)
    return returnTuple

"""Updates this server's distance vector using the Bellman-Ford equation.

:param semaphore: A binary semaphore that is used to protect a critical section
"""

def bellmanFord(semaphore):
    global MY_DVECTOR
    global NEIGHBOR_DVECTORS
    semaphore.acquire() # critical section start
    for key in MY_DVECTOR:
        pathCosts = {}
        if key != MY_ID:
            for serverID in NEIGHBOR_DVECTORS:
                if NEIGHBOR_DVECTORS[serverID][key] != inf_cost and LINK_COSTS[serverID] != inf_cost:
                    pathCost = int(LINK_COSTS[serverID] + NEIGHBOR_DVECTORS[serverID][key]) # cost to neighbor + cost from neighbor to destination
                    pathCosts[serverID] = pathCost
                else:
                    pathCosts[serverID] = inf_cost
            if key in LINK_COSTS:
                pathCost = LINK_COSTS[key]
                if pathCost < pathCosts[key]:
                    pathCosts[key] = pathCost
            if len(pathCosts) > 0:
                lowestValue = min(pathCosts.values())
                lowestKey = list(pathCosts.keys())[list(pathCosts.values()).index(lowestValue)] # Find the dictionary key that had the lowest value
            else:
                lowestValue = inf_cost
            MY_DVECTOR[key][1] = lowestValue # path cost
            if lowestValue == inf_cost:
                MY_DVECTOR[key][0] = -1 # no path
            else:
                MY_DVECTOR[key][0] = lowestKey  # next hop
    semaphore.release() #critical section end


"""Sort MY_DVECTOR by Server ID in ascending order. 
"""

# Sort routing table in ascending order by destination-ID and return to update the ROUTING_TABLE
def sortRouting():
    global MY_DVECTOR
    MY_DVECTOR = dict(sorted(MY_DVECTOR.items()))


def printHelp():
    print("\nupdate <server-ID1> <server-ID2> <Link Cost>" +
          "\n\t<server-ID1>, <server-ID2>: Link to be updated." +
          "\n\t<Link Cost>: New cost between source and destination server." +
          "\n\tThird argument can be an integer or \'inf\' for infinity" +
          "\nstep: Send routing update to neighbors immediately." +
          "\npackets: Display number of distance vector (packets) server received since last invocation." +
          "\ndisplay: Display current routing table." +
          "\ndisable <server-ID>: Disable link to a server." +
          "\ncrash: \"Close\" all connections and exit the program.\n")

"""Creates a routing update that can be sent to another server using the socket.send() method, which can only send strings
and bytes. Routing update is in the following format: [Number of update fields] [Server port of sender] [Server IP of sender] 
[Server IP address n] [Server port n] [Server ID n] [Cost n]

:return: The routing update
:rtype: str
"""

def RoutingUpdate():
    global CONNECTION_LIST
    global MY_DVECTOR
    update = str(3 + (5 * int(SERVER_NUM) - 1)) + " " \
             + CONNECTION_LIST[MY_ID][1] + " " \
             + CONNECTION_LIST[MY_ID][0] + " " \
             + CONNECTION_LIST[MY_ID][0] + " " \
             + CONNECTION_LIST[MY_ID][1] + " " \
             + str(MY_ID) + " 0"
    for destinationID in MY_DVECTOR:
        update = update + " " \
                 + CONNECTION_LIST[destinationID][0] + " " \
                 + CONNECTION_LIST[destinationID][1] + " " \
                 + str(destinationID) + " " \
                 + str(MY_DVECTOR[destinationID][1])
    return update


class repeatTimer:
    """A repeating timer that occupies its own thread.

    :param t: Timer interval in seconds
    :param hFunction: The function that will be called whenever the timer triggers
    :param semaphore: A binary semaphore that will be passed to hFunction to protect a critical section
    """
    def __init__(self, t, hFunction, semaphore):
        self.t = t
        self.hFunction = hFunction
        self.thread = threading.Timer(self.t, self.handle_function, [semaphore])
    def handle_function(self, semaphore):
        self.hFunction(semaphore)
        self.thread = threading.Timer(self.t, self.handle_function, [semaphore])
        self.thread.start()
    def start(self):
        self.thread.start()
    def cancel(self):
        self.thread.cancel()


if __name__ == '__main__':
    print("Enter command: server -t <topology-file-name> -i <routing-update-interval>")
    while True:
        command = input()
        if command.startswith("server"):
            words = command.split(" ")
            if len(words) == 5 and words[1] == "-t":
                filename = words[2]
                if words[3] == "-i":
                    interval = float(words[4])
                    break
            elif len(words) == 5 and words[1] == "-i":
                interval = float(words[2])
                if words[3] == "-t":
                    filename = (words[4])
                    break
        print("Invalid command. Enter: server -t <topology-file-name> -i <routing-update-interval>\n")

    orig_fl = fcntl.fcntl(sys.stdin, fcntl.F_GETFL)
    fcntl.fcntl(sys.stdin, fcntl.F_SETFL, orig_fl | os.O_NONBLOCK)  # set sys.stdin non-blocking
    sel.register(fileobj=sys.stdin, events=selectors.EVENT_READ, data="stdin")
    binSem = Semaphore(1)

    resolveTopology(filename)  # reads topology text file and organizes accordingly
    bootServer()

    timer = repeatTimer(interval, intervalUpdate, binSem)
    timer.start()

    print("Enter \"help\" for more info.\n")
    while True:
        events = sel.select()
        for key, mask in events:
            if key.data == "stdin":
                command = key.fileobj.read()
                command = command.strip()
                if command == "help":
                    printHelp()
                elif "update" in command:  # should give three arguments
                    linkUpdateHelper(command.rstrip(), binSem)
                elif command == "step":
                    stepUpdate()
                elif command == "packets":
                    displayPackets()
                elif command == "display":
                    displayRouting()
                elif "disable" in command:  # should give one argument
                    disableLink(command.rstrip(), binSem)
                elif command == "crash":
                    crashServer()
                else:
                    print("Invalid command. Type \"help\" for more info.\n")
            elif key.data == "listener":
                accept_wrapper(key.fileobj)
            else:
                service_connection(key, mask, binSem)
# main()
